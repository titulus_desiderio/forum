const gulp = require('gulp');
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');
const addsrc = require('gulp-add-src');
const gulpif = require('gulp-if');
const stylus = require('gulp-stylus');
const cssnano = require('gulp-cssnano');
const browserify = require('browserify');
const babelify = require("babelify");
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const uglify = require('gulp-uglify');
const browserSync = require('browser-sync');
require('shelljs/global');

// JS
gulp.task('js', function() {
  return browserify('./src/js/app.js', { debug: true })
    .transform(babelify, {
      presets: ['es2015', 'react', 'stage-0'],
      sourceMaps: true,
    })
    .bundle()
    .on('error', function(err) {
      console.error(err);
      this.emit('end');
    })
    .pipe(source('bundle.min.js'))
    .pipe(buffer())
    .pipe(gulp.dest('./public/js'))
});


// HTML
gulp.task('html', function() {
  return gulp.src('./src/*.html')
    .pipe(gulp.dest('./public'))
});


gulp.task('reload:js', ['js'], browserSync.reload);
gulp.task('reload:html', ['html'], browserSync.reload);
gulp.task('watch', ['build'], function () {
  browserSync({
    notify: true,
    logPrefix: 'BS',
    proxy: `localhost:3200`
  });

  gulp.watch('./src/js/**/*.*', ['reload:js'])
  gulp.watch('./src/*.html', ['reload:html'])
});

gulp.task('build', [
  'js',
  'html',
]);

gulp.task('default', [
  'watch',
]);
