import React  from 'react';
import { connect } from 'react-redux';

import Messages from '../components/Messages.jsx';
import Form from '../components/Form.jsx';

import * as wall from '../actions/wall.js';

class Index extends React.Component {
  constructor (props) {
    super(props);
  }

  componentWillMount() {
    const {
      wall,
      loadMessages,
    } = this.props;

    if (wall.posts.length === 0
     && !wall.fetch)
     loadMessages();

  }

  render() {
    const {
    } = this.props;

    return (
      <div>
        <Form />
        <Messages />
      </div>
    );
  }
};

const mapStateToProps = ({
  wall,
}) => ({
  wall,
});

const mapDispatchToProps = {
  ...wall,
};

export default connect(mapStateToProps, mapDispatchToProps)(Index);
