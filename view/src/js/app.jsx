import React  from 'react';
import { connect } from 'react-redux';

import * as wall from './actions/wall.js';

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      children,
      wall
    } = this.props;

    return (
      <div className="app">
        <h1>Супер тема.</h1>
        {children}
      </div>
    );
  }
};

const mapStateToProps = ({
}) => ({
});

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
