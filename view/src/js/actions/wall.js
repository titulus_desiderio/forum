import { browserHistory } from 'react-router';

import {
  WALL_SENDPOST,
  WALL_LOADED,
  WALL_ERROR,
} from '../reducers/wall.js';

export const loadMessages = () => dispatch => Promise.resolve()
  .then(() => fetch('/message').then(res => res.json()))
  .then(messages => dispatch({
    type: WALL_LOADED,
    payload: messages,
  }))
  .catch(error => {
    console.error(error);
    dispatch({
      type: WALL_ERROR,
      payload: error,
    })
  });

export const sendMessage = data => dispatch => Promise.resolve()
  .then(() => {
    const body = JSON.stringify(data);
    return fetch('/message', {
      method: 'POST',
      body,
      headers: {
        'Content-Type': 'application/json',
      }
    });
  })
  .then(result => dispatch({
    type: WALL_SENDPOST,
  }))
  .then(() => loadMessages()(dispatch))
  .catch(error => {
    console.error(error);
    dispatch({
      type: WALL_ERROR,
      payload: error,
    })
  });
