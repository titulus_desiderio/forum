import React  from 'react';
import { connect } from 'react-redux';

const Message = ({title, body}) => {
    return (
      <div className="message">
          <p className="messages__title">{title}</p>
          <p className="messages__content ">{body}</p>
      </div>
    );
};

class Messages extends React.Component {
  constructor (props) {
    super(props);
  }

  render() {
    const {
      posts
    } = this.props;

    return (
      <div className="messages">
        {
          posts.map(({title,body}, id) => <Message
            key={id}
            title={title}
            body={body}
          />)
        }
        <p className="messages__count "><strong>Количество сообщений:</strong>{posts.length}</p>
      </div>
    );
  }
};

const mapStateToProps = ({
  wall: {posts},
}) => ({
  posts,
});

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(Messages);
