export const WALL_SENDPOST = 'WALL_SENDPOST';
export const WALL_LOADED = 'WALL_LOADED';
export const WALL_ERROR = 'WALL_ERROR';

const initialState = {
  fetch: null,
  posts: [],
  error: null,
};

const wall = (state = initialState, {type, payload} ) => {
  switch (type) {

    case WALL_LOADED:
      return {
        ...state,
        fetch: 'done',
        posts: payload,
      };

    case WALL_SENDPOST:
      return {
        ...state,
        ...payload,
      };

    case WALL_ERROR: {
      return {
        ...state,
        fetch: 'error',
        error: payload,
      }
    }

    default:
      return state;
  };
};

export default wall;
